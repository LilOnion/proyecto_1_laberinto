# import turtle and math libraries
# coding: utf8
import turtle
import math
# define the setings for the window
window = turtle.Screen()
window.bgcolor("black")
window.title("Laberinto")
window.setup(700,700)
#700 x 700 pixels

# define Pen, element for "drawing" the map on to the screen
class Pen(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square")
# the square is 24 by 24 pixels
        self.color("white")
        self.penup()
        self.speed(0)
        
# define Player, the player's token
class Player(turtle.Turtle):
    def __init__(self):
        turtle.Turtle.__init__(self)
        self.shape("square")
        self.color("blue")
        self.speed(0)
        self.gold=0
# define  the moves of the token     
    def go_up(self):
# calculate the position that the token is going to go to
        move_to_x = player.xcor()
        move_to_y = player.ycor() + 24
# check if there's a wall in that coordinate
        if (move_to_x, move_to_y) not in walls:
            self.goto(move_to_x, move_to_y)
            
    def go_down(self):
        move_to_x = player.xcor()
        move_to_y = player.ycor() - 24
        if (move_to_x, move_to_y) not in walls:
            self.goto(move_to_x, move_to_y)
        
    def go_left(self):
        move_to_x = player.xcor() - 24
        move_to_y = player.ycor()
        if (move_to_x, move_to_y) not in walls:
            self.goto(move_to_x, move_to_y)

    def go_right(self):
        move_to_x = player.xcor()+24
        move_to_y = player.ycor()
        if (move_to_x, move_to_y) not in walls:
            self.goto(move_to_x, move_to_y)
            
    def is_collision(self,other):
        a = self.xcor() - other.xcor()
        b = self.ycor() - other.ycor()
# use the Pitagoras theoreme to calculate the distance
# between the player's token and the coin
        distance = math.sqrt((a**2) + (b**2))
# set a condition for when the two icons are overlaping
        if distance < 5:
            return True
        else:
            return False

class Coin(turtle.Turtle):
# initiate the coin in a coordinate
    def __init__(self, x, y):
        turtle.Turtle.__init__(self)
        self.shape("circle")
        self.color("gold")
        self.penup()
        self.speed(0)
        self.gold=100
        self.goto(x, y)
# set a location outside the window to send the coin 
# an also hide it so that it's not visible anymore    
    def destroy(self):
        self.goto(2000,2000)
        self.hideturtle()
       
# create a levels empty list
levels = [""]

# create the level 1 template
level_1 = [
         "XXXXXXXXXXXXXXXXXXXXXXXXX",
         "XPX      X            X X",
         "X   XXXX X X XXXXXX X   X",
         "X XXXXXX X X XX XXX XXXXX",
         "X      X X X XX XX     XX",
         "X XXXX X X X XX    XXX XX",
         "X    X X X XXXXXXX X   XX",
         "XXXX X XXX    XX X X XXXX",
         "X  X X     XX XX X X   XX",
         "X XXXXX XXXXXXXX XXXXX XX",
         "X   XX    XXXXXX X XX  XX",
         "XXX    XX        X X  XXX",
         "X XXXX XXXXX XX XX   XX X",
         "X XX X X     XX XXXXXXX X",
         "X X  XXXXXXXXXX XXXXX   X",
         "X   XX                XXX",
         "XXX    XX X XXXXXXXXXXXXX",
         "X XXXXXXX X X   X   XX XX",
         "X XXXXXXX X X X X X XX XX",
         "T       X X   X   X     X",
         "XXX XXXXX XXXXXXXXXXXXX X",
         "XX  XXXXXXXXX XXXXXXXXX X",
         "XX XX      XX XX        X",
         "XX    XXXX       XXXXXXXX",
         "XXXXXXXXXXXXXXXXXXXXXXXXX"
]

# create a coins list
coins = []

# add level_1 lo levels list
levels.append(level_1)

# define a function to start the board and print it
def setup_maze(level):
# check each position vertically and horizontally
    for y in range(len(level)):
        for x in range(len(level[y])):
# the coordinates are set y,x so that the template
# starts printing from the top left
            character = level[y][x]
# calculate the x and y position in the window
            screen_x = -288 + (x*24)
            screen_y = 288 - (y*24)
            
# check each character in the list and
# set the conditions for each character thats in the list
            if character == "X":
                pen.goto(screen_x, screen_y)
# the stamp function prints the designated icon
                pen.stamp()
# the printed coordinates are added to walls list
                walls.append((screen_x, screen_y))
                
            if character == "P":
# send the player's token to the start point
                player.goto(screen_x, screen_y)
                
            if character == "T":
# add the coin location to the coins list
                coins.append(Coin(screen_x, screen_y))
                
# create variables with the properties of the class elements
pen = Pen()
player = Player()

# create a walls list to save the coordinates of each walls token 
walls = []

# start up the game calling the setup_maze function 
# using the levels list with level_1 inside, as parameters for the function
setup_maze(levels[1])
# make the program read each key pressed on the keyboard
# and set the keybindings
turtle.listen()
turtle.onkey(player.go_left,"Left")
turtle.onkey(player.go_right,"Right")
turtle.onkey(player.go_up,"Up")
turtle.onkey(player.go_down,"Down")

# stop the window's aniamtion once the maze is ready
window.tracer(0)

# bucle for keeping the game running until the player colects all the coins
while (player.gold==0):
# check if the player icon is in collision with the coin thats in
# the coins list
    for coin in coins:
        if player.is_collision(coin):
            player.gold += coin.gold
# destroy the coin
            coin.destroy()
# remove the colected coin from coins list
            coins.remove(coin)

# refresh de window, so that the gold coin is now gone
    window.update()
  #  window.quit()
    
if (player.gold == 100):
    final_message = turtle.Screen()
    final_message.bgcolor("white")
    final_message.title("¡  ¡ F E L I C I D A D E S ,  H A S  GA N A D O !  !")
    final_message.setup(800,20)
    
    final_message.tracer(0)
